<?php
/**
 * Plugin Name: Imagined WordPress Loader
 * Description: A WordPress Must Use Plugin to auto install frequently used plugins and overwrite styles, scripts and functions as a grandchild theme.
 * Author: Imagined | Robin Withaar
 * Version:	1.0
 */

// Test if WordPress is loaded correctly
if (!defined('WPMU_PLUGIN_DIR')) {
    die('WordPress was not loaded! Put this code in the "/wp-content/mu-plugins/" directory.');
}

// Set environment variable: local | test | production
if (!defined('ENV')) {
    define('ENV', 'local');
}

// Set Directory separator shortcut
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

// Echo a var_dump in nice pre tags
function pre_dump(...$var): string
{
    ob_start();
    echo '<pre>';
    var_dump(...$var);
    echo '</pre>';
    return ob_get_clean();
}
function dump(...$var): void
{
    echo pre_dump(...$var);
}

// Get the storage path of a file
function file_path(...$path): string
{
    array_unshift($path, __DIR__);
    return implode(DS, $path);

}

// Get the public url of a file
function public_url(...$path): string
{
    $version = null;
    if ($path[count($path)-1] === true) {
        array_pop($path);
        $file = file_path(...$path);
        if (file_exists($file)) {
            $version = '?v=' . @filemtime($file);
        }
    }
    if(WPMU_PLUGIN_URL !== null) {
        array_unshift($path, WPMU_PLUGIN_URL);
    } else {
        array_unshift($path, (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . '://'.$_SERVER['HTTP_HOST']);
    }
    return implode('/', $path) . $version;
}

// Include all functions
foreach (glob(file_path('functions', '*.php')) as $file) {
    include_once($file);
}
