<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
    <title>Imagined</title>
    <style>
        h1 {
            display: none;
        }
        svg {
            display: block;
            margin: 0 auto;
            width: 70vw;
            height: 50vw;
        }
        svg image {
            width: 70vw;
            height: auto;
        }
    </style>
</head>
<body>
<div class="content">
    <a href="https://imagined.nl">
        <h1>Imagined</h1>
        <svg>
            <image xlink:href="//imagined.nl/coming-soon-imagined.svg" src="//imagined.nl/coming-soon-imagined.png" loading="lazy" />
        </svg>
    </a>
</div>
<?php wp_footer(); ?>
</body>
</html>