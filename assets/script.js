/*!
 * Plugin Name: Imagined WordPress Loader custom JavaScript
 * Description: Include custom js as grandchild theme
 * Author:      Imagined | Robin Withaar
 * Version:     1.0
 * Text Domain: imagined
 * Tags:        script,js,theme
 */

(function($) {
	'use strict';
	// Place custom scripts here.
})(jQuery);
