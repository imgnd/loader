<?php

namespace Imgnd\Loader;

class EnqueueMaster
{
    /**
     * List of styles to include
     * @var array
     */
    public static array $styles = [
        [
            'name' => 'master-style',
            'file' => ['assets', 'style.css'],
            'deps' => []
        ]
    ];

    /**
     * List of scripts to include
     * @var array
     */
    public static array $scripts = [
        [
            'name' => 'master-script',
            'file' => ['assets', 'script.js'],
            'deps' => ['jquery'],
            'foot' => true
        ]
    ];

    public function __construct()
    {
        // Load styles and scripts only on frontend and not in admin
        if (is_admin()) {
            return;
        }
        $this->addAssets('style');
        $this->addAssets('script');
    }

    /**
     * Enqueue assets
     * @param string $type
     */
    private function addAssets(string $type): void
    {
        $array = $type . 's';
        $register = 'wp_register_' . $type;
        $enqueue = 'wp_enqueue_' . $type;
        foreach (self::$$array as $asset) {
            $version = null;
            if (!filter_var($asset['file'], FILTER_VALIDATE_URL)) {
                if (is_array($asset['file'])) {
                    $asset['file'] = file_path(...$asset['file']);
                }
                $version = @filemtime($asset['file']);
                $asset['file'] = plugins_url(basename($asset['file']), $asset['file']);
            }
            $register($asset['name'], $asset['file'], $asset['deps'], $version);
            $enqueue($asset['name']);
        }
    }
}

new EnqueueMaster();
