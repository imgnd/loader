<?php

namespace Imgnd\Loader;

class WpController
{
    public static object $user;

    public function __construct()
    {
        add_action('init', [$this, 'getUser']);
        /** Disable WordPress auto update notifications */
        add_filter('auto_core_update_send_email', '__return_false');
        add_filter('auto_plugin_update_send_email', '__return_false');
        add_filter('auto_theme_update_send_email', '__return_false');
    }

    public function getUser()
    {
        self::$user = wp_get_current_user();
    }
}

new WpController();
