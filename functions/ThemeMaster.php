<?php

namespace Imgnd\Loader;

class ThemeMaster
{
    public function __construct()
    {
        add_filter('template_include', [$this, 'includeTemplate'], 99);
    }

    /**
     * Load custom template when exist and set placeholder for test environment
     * @param string $template
     * @return string
     */
    public function includeTemplate(string $template): string
    {
        $templateFile = file_path('templates', basename($template));
        if (file_exists($templateFile)) {
            $template = $templateFile;
        }
        if (ENV == 'test' && !is_user_logged_in()) {
            $template = file_path('templates', 'placeholder.php');
        }
        return $template;
    }

}

new ThemeMaster();
