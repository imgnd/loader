<?php

namespace Imgnd\Loader;

class AdminMaster
{
    public function __construct()
    {
        add_action('login_head', [$this, 'customLoginLogo']);
        add_action('admin_init', [$this, 'addEditorStyles']);
    }

    /** Set custom WordPress admin login header logo */
    public function customLoginLogo(): void
    {
        echo file_get_contents(file_path('templates', 'login-logo.html'));
    }

    /** Add theme styles and extra admin styles to the WordPress editor */
    public function addEditorStyles(): void
    {
        add_editor_style(get_stylesheet_directory_uri() . DS . 'style.css');
        foreach (EnqueueMaster::$styles as $asset) {
            $version = null;
            if (!filter_var($asset['file'], FILTER_VALIDATE_URL)) {
                if (is_array($asset['file'])) {
                    $asset['file'] = file_path(...$asset['file']);
                }
                $version = @filemtime($asset['file']);
                $asset['file'] = plugins_url(basename($asset['file']), $asset['file']);
            }
            add_editor_style($asset['file']);
        }
    }
}

new AdminMaster();
